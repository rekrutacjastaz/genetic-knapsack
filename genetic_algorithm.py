import random
import numpy as np
import itertools
import operator


class _Chromosome:
    def __init__(self, genes):
        self.genes = genes
        self.weight = 0
        self.value = 0

    def __str__(self):
        return '{} -> weight: {:6.2f}, value: {:6.2f}'.format(self.genes, self.weight, self.value)

    def __repr__(self):
        return '\n' + self.__str__()


def _random_chromosome(size):
    return _Chromosome([random.randint(0, 1) for i in range(size)])


def _generate_population(population_size, chromosome_size):
    population = []
    for i in range(population_size):
        population.append(_random_chromosome(chromosome_size))
    return population


def _crossover(a, b):
    crossover_point = random.randint(1, len(a.genes) - 1)
    x = _Chromosome(a.genes[:crossover_point] + b.genes[crossover_point:])
    y = _Chromosome(b.genes[:crossover_point] + a.genes[crossover_point:])
    return x, y


def _mutation(mutation_chance, *chromosomes):
    if not mutation_chance > 0:
        return chromosomes
    for chromosome in chromosomes:
        size = len(chromosome.genes)
        mutation_flag = np.random.choice([0, 1], size=size, p=[1 - mutation_chance, mutation_chance])
        for i in xrange(size):
            if mutation_flag[i]:
                chromosome.genes[i] = ~chromosome.genes[i] & 1


def _fitness(items, weight_limit, *chromosomes):
    for chromosome in chromosomes:
        taken_items = set(itertools.compress(items, chromosome.genes))
        chromosome.weight = sum(item.weight for item in taken_items)
        chromosome.value = 0
        if chromosome.weight <= weight_limit:
            chromosome.value = sum(item.value for item in taken_items)


def _sorted(chromosomes):
    return sorted(chromosomes, key=operator.attrgetter('value'), reverse=True)


def _extract_elite(population, elitism):
    return _sorted(population)[:elitism]


def _get_best_solution(population):
    return _sorted(population)[0]


def _population_summary(population):
    sorted_population = _sorted(population)
    best = {'chromosome': sorted_population[0].genes, 'value': sorted_population[0].value,
            'weight': sorted_population[0].weight}
    mean = np.mean([chromosome.value for chromosome in sorted_population])
    worst = sorted_population[-1].value
    return best, mean, worst


def _roulette_wheel_selection(population, quantity, mutation_chance, items, weight_limit):
    divisor = sum(chromosome.value for chromosome in population)
    if divisor == 0:
        n = len(population)
        probability = [1.0 / n] * n
    else:
        probability = map(lambda x: x / divisor, list(chromosome.value for chromosome in population))
    new_population = []
    for i in xrange(quantity):
        a, b = np.random.choice(population, size=2, p=probability)
        x, y = _crossover(a, b)
        _mutation(mutation_chance, x, y)
        _fitness(items, weight_limit, x, y)
        better_offspring = _get_best_solution([x, y])
        new_population.append(better_offspring)
    return new_population


def _tournament_selection(population, quantity, mutation_chance, items, weight_limit, tournament_size=4):
    new_population = []
    for i in xrange(quantity):
        selection = random.sample(population, tournament_size)
        a = _sorted(selection)[0]
        selection = random.sample(population, tournament_size)
        b = _sorted(selection)[0]
        x, y = _crossover(a, b)
        _mutation(mutation_chance, x, y)
        _fitness(items, weight_limit, x, y)
        better_offspring = _get_best_solution([x, y])
        new_population.append(better_offspring)
    return new_population


def gen_alg(items, weight_limit, population_size, chromosome_size, generations, mutation_chance, elitism, selection):
    if selection == 'roulette':
        selection = _roulette_wheel_selection
    elif selection == 'tournament':
        selection = _tournament_selection

    population = _generate_population(population_size, chromosome_size)
    for chromosome in population:
        _fitness(items, weight_limit, chromosome)
    best, mean, worst = _population_summary(population)
    yield {'generation': 1, 'best': best, 'mean': mean, 'worst': worst}
    for generation in xrange(generations-1):
        elite = _extract_elite(population, elitism)
        rest = selection(population, len(population) - len(elite), mutation_chance, items, weight_limit)
        population = elite + rest
        best, mean, worst = _population_summary(population)
        yield {'generation': generation + 2, 'best': best, 'mean': mean, 'worst': worst}
