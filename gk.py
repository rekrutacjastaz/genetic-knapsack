from parse_args import parse_args
from csv_reader import get_items
from genetic_algorithm import gen_alg
import plot
from csv_writer import save_output
import itertools

(file_path, weight_limit, population_size, generations, mutation_chance, elitism, solution_method, draw_plot,
 best_solution, output) = parse_args()

items = get_items(file_path)
chromosome_size = len(items)

if mutation_chance is None:
    mutation_chance = 1 / (chromosome_size + 1)

if elitism is None:
    elitism = int(0.2 * population_size)

solutions = list(gen_alg(items, weight_limit, population_size, chromosome_size, generations, mutation_chance, elitism,
                         solution_method))
if draw_plot:
    plot.draw([[solution['generation'], solution['best']['value']] for solution in solutions], best_solution)
if output:
    save_output(output, solutions)

result = solutions[-1]['best']
print 'Value: {}'.format(result['value'])
print 'Weight: {} (limit: {})'.format(result['weight'], weight_limit)
taken_items = set(itertools.compress(items, result['chromosome']))
print '\nTaken items:'
for item in taken_items:
    print item.name
