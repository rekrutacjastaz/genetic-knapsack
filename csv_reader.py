import csv
import sys


class _Item:
    def __init__(self, name, weight, value):
        self.name = name
        self.weight = weight
        self.value = value

    def __str__(self):
        return '[{}: ${}, {}kg]'.format(self.name, self.value, self.weight)

    def __repr__(self):
        return self.__str__()


def get_items(csv_file):
    try:
        with open(csv_file) as data:
            dict_reader = csv.DictReader(data)
            items = []
            for d in dict_reader:
                try:
                    items.append(_Item(d['item'], float(d['weight']), float(d['value'])))
                except KeyError as e:
                    print 'Error: Wrong CSV file header (should be: item,weight,value)'
                    sys.exit(1)
    except IOError as e:
        print e
        sys.exit(1)

    return items
