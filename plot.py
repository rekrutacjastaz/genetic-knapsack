import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.patches as patches


def _init():
    ax.set_ylim(0, 10)
    ax.set_xlim(0, 10)
    ax.grid()
    return line


def _update(data):
    x, y = data
    x_data.append(x)
    y_data.append(y)

    x_min, x_max = ax.get_xlim()
    y_min, y_max = ax.get_ylim()
    if x >= x_max:
        ax.set_xlim(x_min, 2 * x_max)
        ax.figure.canvas.draw()
    if y >= y_max:
        ax.set_ylim(y_min, 2 * y)
        ax.figure.canvas.draw()

    line.set_data(x_data, y_data)
    return line,


fig, ax = plt.subplots()
line, = ax.plot([], [], linewidth=1)
x_data, y_data = [], []


def draw(function, best_solution=None):
    paths = []
    if best_solution is not None:
        ax.axhline(y=best_solution, c="red", linewidth=1)
        paths.append(patches.Patch(color='red', label='Best solution'))
    paths.append(patches.Patch(color='blue', label='Generation best solution'))
    plt.legend(handles=paths)
    ax.set_xlabel('generation')
    ax.set_ylabel('value')

    ani = animation.FuncAnimation(fig, _update, function, init_func=_init, repeat=False)
    plt.show()
