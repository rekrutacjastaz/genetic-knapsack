import argparse


def weight_limit_type(x):
    x = float(x)
    if x < 1:
        raise argparse.ArgumentTypeError('The weight limit must be at least 1')
    return x


def population_type(x):
    x = int(x)
    if x < 5:
        raise argparse.ArgumentTypeError('The population size must be at least 5')
    return x


def generations_type(x):
    x = int(x)
    if x < 1:
        raise argparse.ArgumentTypeError('The number of generations must be at least 1')
    return x


def mutation_type(x):
    x = float(x)
    if not 0 <= x <= 1:
        raise argparse.ArgumentTypeError('Mutation chance is a floating point number from 0 to 1 inclusive: [0,1]')
    return x


def best_solution_type(x):
    x = float(x)
    if not x > 0:
        raise argparse.ArgumentTypeError('Best solution must be greater than 0')
    return x


def parse_args():
    """Parse script arguments.

    :returns:
        A tuple with: file_path, weight_limit, population_size, generations, mutation_chance, elitism, selection_method,
        draw_plot, best_solution and output
    """
    parser = argparse.ArgumentParser(description='Solve the knapsack problem using genetic algorithm')
    parser.add_argument('input', help='CSV file with items (header: item,weight,value)')
    parser.add_argument('weight_limit', type=weight_limit_type, help='the knapsack\'s capacity')
    parser.add_argument('population_size', type=population_type, help='the population size (>4)')
    parser.add_argument('generations', type=generations_type, help='the number of generations (>0)')

    parser.add_argument('-m', '--mutation-chance', type=mutation_type,
                        help='Chance that a gene in the chromosome mutates. By default 1/(chromosome_size + 1).')
    parser.add_argument('-e', '--elitism', type=int,
                        help='Number of best chromosomes that are kept into the next generation. By default is about '
                             '20%% of the population size.')
    parser.add_argument('-s', '--selection-method', type=str, choices=['roulette', 'tournament'], default='roulette',
                        help='selection method (default: roulette)')
    parser.add_argument('-d', '--draw-plot', action='store_true', help='draw animated plot')
    parser.add_argument('-b', '--best-solution', type=best_solution_type, help='show best solution on the plot')
    parser.add_argument('-o', '--output', help='output CSV file')

    args = parser.parse_args()

    if not args.population_size > args.elitism:
        parser.error('argument -e/--elitism: The elitism must be less than the population size')
    if args.best_solution and not args.draw_plot:
        parser.error('argument -b/--best-solution: Best solution can not be used without -d/--draw-plot')

    return (args.input, args.weight_limit, args.population_size, args.generations, args.mutation_chance, args.elitism,
            args.selection_method, args.draw_plot, args.best_solution, args.output)
