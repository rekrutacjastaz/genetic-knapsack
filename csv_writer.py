import csv
import sys


def save_output(csv_file, data):
    try:
        with open(csv_file, 'wb') as output_file:
            writer = csv.writer(output_file)
            writer.writerow(['generation', 'best', 'mean', 'worst'])
            for d in data:
                row = [d['generation'], d['best']['value'], d['mean'], d['worst']]
                writer.writerow(row)
    except IOError as e:
        print e
        sys.exit(1)
